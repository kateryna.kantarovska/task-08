package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Controller for SAX parser.
 */
public class SAXController extends DefaultHandler {
	
	private final String xmlFileName;
	private static final ArrayList<Flower> flowersList = new ArrayList<>();
	private String currentElement;
	short currentIndex = -1;
	private String aveLenFlowerAttribute = "";
	private String temperatureAttribute = "";
	private String wateringAttribute = "";

	public SAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<Flower> getFlowersList() {
		return flowersList;
	}

	// PLACE YOUR CODE HERE


	public void parse() throws ParserConfigurationException, SAXException, IOException {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		SAXParser parser = factory.newSAXParser();
		SAXController saxController = new SAXController(xmlFileName);
		parser.parse(new File(xmlFileName), saxController);
	}

	public void sort() {
		flowersList.sort(Comparator.comparing(Flower::getAveLenFlower));
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) {
		currentElement = qName;

		if (currentElement.equals("flower")) {
			currentIndex++;
			flowersList.add(new Flower());
		}
		if (currentElement.equals("aveLenFlower")) {
			aveLenFlowerAttribute += attributes.getValue("measure");
		}
		if (currentElement.equals("temperature")) {
			temperatureAttribute += attributes.getValue("measure");
		}
		if (currentElement.equals("lighting")) {
			flowersList.get(currentIndex).setLighting(attributes.getValue("lightRequiring"));
		}
		if (currentElement.equals("watering")) {
			wateringAttribute += attributes.getValue("measure");
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) {
		currentElement = "";
		if (qName.equals("aveLenFlower")) {
			aveLenFlowerAttribute = "";
		}
		if (qName.equals("temperature")) {
			temperatureAttribute = "";
		}
		if (qName.equals("watering")) {
			wateringAttribute = "";
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) {
		String info = new String(ch, start, length).trim();
		if (!info.isEmpty()) {
			if (currentElement.equals("name")) {
				flowersList.get(currentIndex).setName(info);
			}
			if (currentElement.equals("soil")) {
				flowersList.get(currentIndex).setSoil(info);
			}
			if (currentElement.equals("origin")) {
				flowersList.get(currentIndex).setOrigin(info);
			}
			if (currentElement.equals("stemColour")) {
				flowersList.get(currentIndex).setStemColour(info);
			}
			if (currentElement.equals("leafColour")) {
				flowersList.get(currentIndex).setLeafColour(info);
			}
			if (currentElement.equals("aveLenFlower")) {
				flowersList.get(currentIndex).setAveLenFlower(info + " " + aveLenFlowerAttribute);
			}
			if (currentElement.equals("temperature")) {
				flowersList.get(currentIndex).setTemperature(info + " " + temperatureAttribute);
			}
			if (currentElement.equals("watering")) {
				flowersList.get(currentIndex).setWatering(info + " " + wateringAttribute);
			}
			if (currentElement.equals("multiplying")) {
				flowersList.get(currentIndex).setMultiplying(info);
			}
		}
	}
}