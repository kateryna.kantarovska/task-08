package com.epam.rd.java.basic.task8.controller;


import com.epam.rd.java.basic.task8.Flower;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Controller for DOM parser.
 */
public class DOMController {

	private final String xmlFileName;
	private final ArrayList<Flower> flowersList = new ArrayList<>();

	public DOMController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}


	public ArrayList<Flower> getFlowersList() {
		return flowersList;
	}

	// PLACE YOUR CODE HERE
	public ArrayList<Flower> parseAndStore() throws ParserConfigurationException, IOException, SAXException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.parse(new File(xmlFileName));
		document.getDocumentElement().normalize();
		NodeList flowers = document.getDocumentElement().getElementsByTagName("flower");
		for (int i = 0; i < flowers.getLength(); i++) {
			Node flower = flowers.item(i);
			if (flower.getNodeType() == Node.ELEMENT_NODE) {
				Element flowerElement = (Element) flower;
				String name = flowerElement.getElementsByTagName("name").item(0).getTextContent();
				String soil = flowerElement.getElementsByTagName("soil").item(0).getTextContent();
				String origin = flowerElement.getElementsByTagName("origin").item(0).getTextContent();
				String multiplying = flowerElement.getElementsByTagName("multiplying").item(0).getTextContent();
				NodeList visualParameters = flowerElement.getElementsByTagName("visualParameters").item(0).getChildNodes();
				String stemColour = "";
				String leafColour = "";
				String aveLenFlower = "";
				for (int j = 0; j < visualParameters.getLength(); j++) {
					if (visualParameters.item(j).getNodeType() == Node.ELEMENT_NODE) {
						Element parameter = (Element)visualParameters.item(j);
						switch (parameter.getNodeName()) {
							case "stemColour": stemColour = parameter.getTextContent();
							break;
							case "leafColour": leafColour = parameter.getTextContent();
							break;
							case "aveLenFlower": aveLenFlower = parameter.getTextContent() + " " + parameter.getAttribute("measure");
							break;
							default:
						}
					}
				}
				NodeList growingTips = flowerElement.getElementsByTagName("growingTips").item(0).getChildNodes();
				String temperature = "";
				String lighting = "";
				String watering = "";
				for (int j = 0; j < growingTips.getLength(); j++) {
					if (growingTips.item(j).getNodeType() == Node.ELEMENT_NODE) {
						Element tip = (Element) growingTips.item(j);
						switch (tip.getNodeName()) {
							case "temperature": temperature = tip.getTextContent() + " " + tip.getAttribute("measure");
							break;
							case "lighting": lighting = tip.getAttribute("lightRequiring");
							break;
							case "watering": watering = tip.getTextContent() + " " + tip.getAttribute("measure");
							break;
							default:
						}
					}
				}
				flowersList.add(new Flower(name, soil, origin, stemColour, leafColour, aveLenFlower, temperature, lighting, watering, multiplying));
			}
		}
		return flowersList;
	}

	public void sort() throws ParserConfigurationException, IOException, SAXException {
		parseAndStore().sort(Comparator.comparing(Flower::getName));
	}

	public void saveToXml (String outputXmlFile, ArrayList<Flower> flowersList) throws ParserConfigurationException, TransformerException {
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		DocumentBuilder builder = factory.newDocumentBuilder();
		Document document = builder.newDocument();
		Element root = document.createElementNS("http://www.nure.ua", "flowers");
		document.appendChild(root);
		for (Flower flowerObject : flowersList) {
			Element flower = document.createElement("flower");
			root.appendChild(flower);

			Element name = document.createElement("name");
			name.setTextContent(flowerObject.getName());
			flower.appendChild(name);

			Element soil = document.createElement("soil");
			soil.setTextContent(flowerObject.getSoil());
			flower.appendChild(soil);

			Element origin = document.createElement("origin");
			origin.setTextContent(flowerObject.getOrigin());
			flower.appendChild(origin);

			Element visualParameters = document.createElement("visualParameters");

			Element stemColour = document.createElement("stemColour");
			stemColour.setTextContent(flowerObject.getStemColour());
			visualParameters.appendChild(stemColour);

			Element leafColour = document.createElement("leafColour");
			leafColour.setTextContent(flowerObject.getLeafColour());
			visualParameters.appendChild(leafColour);

			Element aveLenFlower = document.createElement("aveLenFlower");
			String[] valueAttribute = flowerObject.getAveLenFlower().split(" ");
			aveLenFlower.setAttribute("measure", valueAttribute[1]);
			aveLenFlower.setTextContent(valueAttribute[0]);
			visualParameters.appendChild(aveLenFlower);

			flower.appendChild(visualParameters);

			Element growingTips = document.createElement("growingTips");

			Element temperature = document.createElement("temperature");
			valueAttribute = flowerObject.getTemperature().split(" ");
			temperature.setAttribute("measure", valueAttribute[1]);
			temperature.setTextContent(valueAttribute[0]);
			growingTips.appendChild(temperature);

			Element lighting = document.createElement("lighting");
			lighting.setAttribute("lightRequiring", flowerObject.getLighting());
			growingTips.appendChild(lighting);

			Element watering = document.createElement("watering");
			valueAttribute = flowerObject.getWatering().split(" ");
			watering.setAttribute("measure", valueAttribute[1]);
			watering.setTextContent(valueAttribute[0]);
			growingTips.appendChild(watering);

			flower.appendChild(growingTips);

			Element multiplying = document.createElement("multiplying");
			multiplying.setTextContent(flowerObject.getMultiplying());
			flower.appendChild(multiplying);

			StreamResult result = new StreamResult(new File(outputXmlFile));

			TransformerFactory transformerFactory = TransformerFactory.newInstance();
			Transformer transformer = transformerFactory.newTransformer();
			transformer.setOutputProperty(OutputKeys.INDENT, "yes");

			transformer.transform(new DOMSource(document), result);
		}

	}

}
