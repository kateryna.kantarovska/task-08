package com.epam.rd.java.basic.task8;

public class Flower {
    private String name;
    private String soil;
    private String origin;
    private String stemColour;
    private String leafColour;
    private String aveLenFlower;
    private String temperature;
    private String lighting;
    private String watering;
    private String multiplying;

    public Flower(String name, String soil, String origin, String stemColour, String leafColour, String aveLenFlower, String temperature, String lighting, String watering, String multiplying) {
        this.name = name;
        this.soil = soil;
        this.origin = origin;
        this.stemColour = stemColour;
        this.leafColour = leafColour;
        this.aveLenFlower = aveLenFlower;
        this.temperature = temperature;
        this.lighting = lighting;
        this.watering = watering;
        this.multiplying = multiplying;
    }

    public Flower() {
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSoil(String soil) {
        this.soil = soil;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public void setStemColour(String stemColour) {
        this.stemColour = stemColour;
    }

    public void setLeafColour(String leafColour) {
        this.leafColour = leafColour;
    }

    public void setAveLenFlower(String aveLenFlower) {
        this.aveLenFlower = aveLenFlower;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public void setLighting(String lighting) {
        this.lighting = lighting;
    }

    public void setWatering(String watering) {
        this.watering = watering;
    }

    public void setMultiplying(String multiplying) {
        this.multiplying = multiplying;
    }

    public String getName() {
        return name;
    }

    public String getSoil() {
        return soil;
    }

    public String getOrigin() {
        return origin;
    }

    public String getStemColour() {
        return stemColour;
    }

    public String getLeafColour() {
        return leafColour;
    }

    public String getAveLenFlower() {
        return aveLenFlower;
    }

    public String getTemperature() {
        return temperature;
    }

    public String getLighting() {
        return lighting;
    }

    public String getWatering() {
        return watering;
    }

    public String getMultiplying() {
        return multiplying;
    }



    @Override
    public String toString() {
        return "Flower{" +
                "name='" + name + '\'' +
                ", soil='" + soil + '\'' +
                ", origin='" + origin + '\'' +
                ", stemColour='" + stemColour + '\'' +
                ", leafColour='" + leafColour + '\'' +
                ", aveLenFlower='" + aveLenFlower + '\'' +
                ", temperature='" + temperature + '\'' +
                ", lighting='" + lighting + '\'' +
                ", watering='" + watering + '\'' +
                ", multiplying='" + multiplying + '\'' +
                '}';
    }
}
