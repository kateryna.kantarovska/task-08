package com.epam.rd.java.basic.task8.controller;

import com.epam.rd.java.basic.task8.Flower;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.StartElement;
import javax.xml.stream.events.XMLEvent;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Comparator;

/**
 * Controller for StAX parser.
 */
public class STAXController extends DefaultHandler {

	private final String xmlFileName;
	private final ArrayList<Flower> flowersList = new ArrayList<>();
	static short currentIndex = -1;

	public STAXController(String xmlFileName) {
		this.xmlFileName = xmlFileName;
	}

	public ArrayList<Flower> getFlowersList() {
		return flowersList;
	}

	// PLACE YOUR CODE HERE
	public void parse() throws FileNotFoundException, XMLStreamException {
		XMLInputFactory xmlInputFactory = XMLInputFactory.newInstance();
		XMLEventReader reader = xmlInputFactory.createXMLEventReader(new FileInputStream(xmlFileName));
		while(reader.hasNext()) {
			XMLEvent event = reader.nextEvent();
			if (event.isStartElement()) {
				StartElement startElement = event.asStartElement();
				switch (startElement.getName().getLocalPart()) {
					case "flower": {
						currentIndex++;
						flowersList.add(new Flower());
					}
					break;
					case "name": {
						event = reader.nextEvent();
						flowersList.get(currentIndex).setName(event.asCharacters().getData());
					}
					break;
					case "soil": {
						event = reader.nextEvent();
						flowersList.get(currentIndex).setSoil(event.asCharacters().getData());
					}
					break;
					case "origin": {
						event = reader.nextEvent();
						flowersList.get(currentIndex).setOrigin(event.asCharacters().getData());
					}
					break;
					case "stemColour": {
						event = reader.nextEvent();
						flowersList.get(currentIndex).setStemColour(event.asCharacters().getData());
					}
					break;
					case "leafColour": {
						event = reader.nextEvent();
						flowersList.get(currentIndex).setLeafColour(event.asCharacters().getData());
					}
					break;
					case "aveLenFlower": {
						event = reader.nextEvent();
						String aveLenFlower = event.asCharacters().getData() + " "
								+ startElement.getAttributeByName(new QName("measure")).getValue();
						flowersList.get(currentIndex).setAveLenFlower(aveLenFlower);
					}
					break;
					case "temperature": {
						event = reader.nextEvent();
						String temperature = event.asCharacters().getData() + " "
								+ startElement.getAttributeByName(new QName("measure")).getValue();
						flowersList.get(currentIndex).setTemperature(temperature);
					}
					break;
					case "lighting": {
						flowersList.get(currentIndex)
								.setLighting(startElement.getAttributeByName(new QName("lightRequiring")).getValue());
					}
					break;
					case "watering": {
						event = reader.nextEvent();
						String watering = event.asCharacters().getData() + " "
								+ startElement.getAttributeByName(new QName("measure")).getValue();
						flowersList.get(currentIndex).setWatering(watering);
					}
					break;
					case "multiplying": {
						event = reader.nextEvent();
						flowersList.get(currentIndex).setMultiplying(event.asCharacters().getData());
					}
					break;
					default:
				}
			}
		}
		reader.close();
	}

	public void sort() {
		flowersList.sort(Comparator.comparing(Flower::getOrigin));
	}

}